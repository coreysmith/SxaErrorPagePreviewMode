# SXA Error Page Preview Mode Issue

This is a project to demonstrate an issue with SXA where error pages aren't displayed for sites with `Enable Preview` unchecked.

## Setup

1. Install a new instance of Sitecore 9.0 Update-1.
2. Install `Sitecore PowerShell Extensions-4.7.2 for Sitecore 8.zip` in Sitecore.
3. Install `Sitecore Experience Accelerator 1.7 rev. 180410 for 9.0.zip` in Sitecore.
4. Update the `publishUrl` in the [publish profile][1] to point to your Sitecore installation.
5. Update the `physicalRootPath` in [Unicorn.CustomSerializationFolder.config][2] to point to the correct path on your disk.
6. Publish the solution.
7. Sync Unicorn at `/unicorn.aspx`.
8. **Sync Unicorn again (important!).**
9. Perform a smart site publish.

## Testing the issue

### Real world

1. Navigate to the home page of the site at `/`. Note that the ASP.NET error page is displayed.
2. Open [SxaErrorPagePreviewMode.config][3] and set `XA.Foundation.Multisite.Environment` to `ContentManagement`.
3. Publish the solution.
4. Navigate to the home page of the site at `/`. Note that the custom error page is displayed.
5. Open [SxaErrorPagePreviewMode.config][3] and set `XA.Foundation.Multisite.Environment` to `ContentDelivery`.
6. Publish the solution.
7. Navigate to the home page of the site at `/`. Note that the ASP.NET error page is displayed.

### Isolated

1. Navigate to the home page of the site at `/`. Note that the ASP.NET error page is displayed.
2. Open the `Content Delivery` site grouping for the `Test Site` (item ID `{B5D2B341-25BC-4486-9195-8C53A1BACBC3}`).
3. Check `Enable Preview`. Save the item.
4. Perform a smart site publish.
5. Navigate to the home page of the site at `/`. Note that the custom error page is displayed.
6. Uncheck `Enable Preview` on the `Content Delivery` site grouping.
7. Perform a smart site publish.
8. Navigate to the home page of the site at `/`. Note that the ASP.NET error page is displayed.

[1]: src/SxaErrorPagePreviewMode.Web/Properties/PublishProfiles/PublishProfile.pubxml
[2]: src/SxaErrorPagePreviewMode.Web/App_Config/Include/zzz/Unicorn.CustomSerializationFolder.config
[3]: src/SxaErrorPagePreviewMode.Web/App_Config/Include/zzz/SxaErrorPagePreviewMode.config
